﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace valor_de_un_vector
{
    class Program
    {
     /*Crear un programa que permita
     crear, cargar y obtener el menor y
     mayor valor de un vector. La obtención
     del mayor y menor hacerlo en un único
     método que retorne dichos dos valores.*/



        private int[] VECTOR;

        public Program()
        {

            Console.WriteLine("//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine("Bienvenido, este programa premite obtener el mayor y el menor valor del vector.");

            Console.Write("\nEspecifique la cantidad de valores a ingresar: ");

            int CantidadDEvalores = int.Parse(Console.ReadLine());
            VECTOR = new int[CantidadDEvalores];
        }
        public void Cargar()
        {
            for (var f = 0; f < VECTOR.Length; f++)
            {
                Console.Write("Ingrese valor: ");
                VECTOR[f] = int.Parse(Console.ReadLine());
            }
        }
        public void Mayor_y_Menor(out int MAYOR, out int MENOR)
        {
            MAYOR = VECTOR[0];
            MENOR = VECTOR[0];
            for (var i = 1; i < VECTOR.Length; i++)
            {
                if (VECTOR[i] > MAYOR)
                {
                    MAYOR = VECTOR[i];
                }
                else
                {
                    if (VECTOR[i] < MENOR)
                    {
                        MENOR = VECTOR[i];
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            Program p = new Program();
            p.Cargar();
            int menor, mayor;
            p.Mayor_y_Menor(out menor, out mayor);

            Console.WriteLine("\nEl mayor valor del vector es:" + mayor);
            Console.WriteLine("El menor valor del vector es:" + menor);
            Console.WriteLine("/////////////////////////////////////////");

            Console.WriteLine("     ¡GRACIAS POR PREFERIRNOS!");

            Console.ReadKey();
        }
    }
}