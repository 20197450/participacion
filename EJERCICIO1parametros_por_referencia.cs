﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parametros_por_referencia
{
    class Program
    {
            /*Crear un metodo que retorne 5
            valores random entre 1 y 30 mediante
            parametros por referencia.*/
            public void LosCincoValoresAleatorios(out int VALOR1, out int VALOR2, out int VALOR3, out int VALOR4, out int VALOR5)
            {
                Random r = new Random();
                VALOR1 = r.Next(1, 31);
                VALOR2 = r.Next(1, 31);
                VALOR3 = r.Next(1, 31);
                VALOR4 = r.Next(1, 31);
                VALOR5 = r.Next(1, 31);

            Console.WriteLine("///////////////////////////////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine("  Bienvenido al mejor programa, el mismo le retorna 5 valores random entre los numeros 1 y 30\n");

        }
            static void Main(string[] args)
            {
                int valorA1, valorA2, valorA3, valorA4, valorA5;
            Program p = new Program();


            p.LosCincoValoresAleatorios(out valorA1, out valorA2, out valorA3, out valorA4, out valorA5);


                Console.WriteLine("   Valor 1) aleatorio:" + valorA1);
                Console.WriteLine("   Valor 2) aleatorio:" + valorA2);
                Console.WriteLine("   Valor 3) aleatorio:" + valorA3);
                Console.WriteLine("   Valor 4) aleatorio:" + valorA4);
                Console.WriteLine("   Valor 5) aleatorio:" + valorA5);

            Console.WriteLine("\n¡GRACIAS POR PREFERIRNOS!");

            Console.ReadKey();
            }
        }
    }
